#!/bin/bash

# Tablica obrazów systemu linux w roznych wersjach.
declare -A LINUX_DISTRO_ARRAY=( ["Ubuntu 16.04"]="https://cloud-images.ubuntu.com/xenial/current/xenial-server-cloudimg-amd64-disk1.img" 
                                ["Ubuntu 18.04"]="https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img" 
                                ["Ubuntu 20.04"]="https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img" 
                                ["Quit"]="Quit" 
                                )
VM_HOSTNAME="VM_TEST"                
declare -i VM_CPU=0
declare -i VM_RAM=0
declare -i VM_DISK=0

function create_vm () {
  function clear_vm_set () {
    VM_CPU=0
    VM_RAM=0
    VM_DISK=0
  }
  read -p "Podaj nazwe hosta: " VM_HOSTNAME
  until [ "$VM_CPU" -ge 1 ]
  do
    read -p "Ile CPU: " VM_CPU
  done
  until [ "$VM_RAM" -ge 1 ]
  do
    read -p "Ile RAM(GB): " VM_RAM
  done
  until [ "$VM_DISK" -ge 1 ]
  do
    read -p "Ile GB dla dysku: " VM_DISK
  done
  echo ""
  echo "Maszyna wirtualna: $VM_HOSTNAME (CPU: $VM_CPU  RAM: $VM_RAM  DYSK: $VM_DISK)"
  read -p "Naciśnij Eter by pobrac obraz:"
}

function download_iso () {
  echo ${distro};
  local filename=${distro// /}
  wget ${LINUX_DISTRO_ARRAY["${distro}"]} -q --show-progress -O ./${filename,,}.img 
  echo; echo;
  read -p "Pobranie zakonczone! Nacisnij Enter."
}

# Półapka                                 
#trap 'echo " Aby przerwac i zamknac skrypt wybierz pozycje 4..."' SIGINT SIGTERM SIGTSTP

# Pobieranie obrazu pliku systemu
PS3="Wybierz opcję: "
while true
do   
    select distro in "${!LINUX_DISTRO_ARRAY[@]}"
    do 
        case $distro in
            "Ubuntu 16.04") 
              create_vm
              #download_iso
              #clear_vm_set
              break ;;
            "Ubuntu 18.04")
              create_vm 
              #download_iso
              #clear_vm_set
              break ;;
            "Ubuntu 20.04") 
              create_vm
              #download_iso
              #clear_vm_set
              break ;;
            "Quit") echo "Koniec na dzis"; exit 0 ;;
            *) echo "Nie rozpoznawalna wersja distro."; break ;;
        esac
    done
clear
done