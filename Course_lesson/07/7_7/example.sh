#!/bin/bash

ctrlc_count=0

function no_ctrlc() {
    let ctrlc_count++
    echo
    if [[ $ctrlc_count == 1 ]]; then
        echo "Przestan!"
    elif [[ $ctrlc_count == 2 ]]; then
        echo "Jeszcze raz i wyjde"
    else
        echo "Okej to wychodze!"
        exit
    fi
}

trap no_ctrlc SIGINT

while true
do
    echo "Sleeping"
    sleep 10
done 