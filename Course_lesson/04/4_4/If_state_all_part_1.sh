#!/bin/bash


if [[ $1 = "+" ]]
then
    let wynik=$2+$3
    echo $wynik
elif [[ $1 = "-" ]]
then
    let wynik=$2-$3
    echo $wynik
elif [[ $1 = "x" ]]
then
    let wynik=$2*$3
    echo $wynik
elif [[ $1 = "/" ]]
then 
    let wynik=$2/$3
    echo $wynik
else
    echo "Cos poszlo nie tak sproboj jeszcze raz !!!"
fi