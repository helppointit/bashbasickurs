#!/bin/bash

#1.
for i in 1 2 3 4 5 6 7 8 9 0
do
    echo "Powitanie numer $i"
done

echo ""

# bash >= 4.0
for i in {1..101..10} #poczatek..koniec..iteracje
do
    echo "Petla nr2 numer $i"
done

echo ""

# bash <= 3.+
for i in $(seq 1 2 20) # poczatek iteracja koniec
do
  echo "Petla nr3 numer $i"  
done

echo ""

for (( c=1; c<=5; c+=2 ))
do
    echo "Petla nr4 numer $c"
done